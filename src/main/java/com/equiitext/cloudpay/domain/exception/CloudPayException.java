package com.equiitext.cloudpay.domain.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by Quang on 01/11/17.
 */
public class CloudPayException extends RuntimeException{

    private String key;
    private HttpStatus httpErrorCode;

    public CloudPayException(String key, HttpStatus httpErrorCode) {
        setKey(key);
        setHttpErrorCode(httpErrorCode);
    }

    public HttpStatus getHttpErrorCode() {
        return httpErrorCode;
    }

    private void setHttpErrorCode(HttpStatus httpErrorCode) {
        this.httpErrorCode = httpErrorCode;
    }

    public String getKey() {
        return key;
    }

    private void setKey(String key) {
        this.key = key;
    }
}