package com.equiitext.cloudpay.domain;

import com.equiitext.cloudpay.dao.dto.*;
import com.equiitext.cloudpay.infrastructure.db.BlockerDao;
import com.equiitext.cloudpay.infrastructure.db.ChargeDao;
import com.equiitext.cloudpay.infrastructure.db.CustomerPayDao;
import com.equiitext.cloudpay.infrastructure.db.model.BlockerModel;
import com.equiitext.cloudpay.infrastructure.db.model.ChargeHistoryModel;
import com.equiitext.cloudpay.infrastructure.db.model.CustomerModel;
import com.equiitext.cloudpay.infrastructure.slack.SlackChannel;
import com.equiitext.cloudpay.infrastructure.slack.SlackHelper;
import com.stripe.exception.*;
import com.stripe.model.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class CustomerService {
    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);
    private final int MAX_FAILED = 3;
    @Autowired
    private CustomerPayDao customerPayDao;
    @Autowired
    private ChargeDao chargeDao;
    @Autowired
    private BlockerDao blockerDao;

    @Value("${spring.profiles.active}")
    private String env;

    public CloudPayResponse create(NewCardPayload cardPayload) {
        try {
            Map<String, Object> cardParams = new HashMap<>();
            cardParams.put("source", cardPayload.getToken());

            Map<String, Object> metadata = new HashMap<>();
            if (!StringUtils.isEmpty(cardPayload.getPhone())) {
                metadata.put("phone", cardPayload.getPhone());
            }
            if (!StringUtils.isEmpty(cardPayload.getEmail())) {
                metadata.put("email", cardPayload.getEmail());
            }
            cardParams.put("metadata", metadata);

            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(cardPayload.getExCustId(), cardPayload.getSystem());
            Card newCard;
            if (customerModel != null) { // Add more card
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                newCard = (Card) customer.getSources().create(cardParams);
            } else { // Add new card
                Customer customer = Customer.create(cardParams);
                customerPayDao.save(cardPayload.getExCustId(), customer.getId(), cardPayload.getSystem());
                newCard = (Card) customer.getSources().getData().get(0);
            }
            LOGGER.info("create {}", newCard);
            newCard.setLastResponse(null);
            return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, newCard);
        } catch (Exception e) {
            LOGGER.info("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    public CloudPayResponse get(CardPayload cardPayload) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(cardPayload.getExCustId(), cardPayload.getSystem());
            if (customerModel != null) {
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                String cardId = cardPayload.getCardId();

                if (cardId == null || cardId.isEmpty()) {
                    cardId = customer.getDefaultSource();
                }

                Card source = (Card) customer.getSources().retrieve(cardId);
                source.setLastResponse(null);// Trick to avoid JSON deserializer exception
                LOGGER.info("get {}", source);
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, source);
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    public CloudPayResponse setDefault(CardPayload cardPayload) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(cardPayload.getExCustId(), cardPayload.getSystem());
            if (customerModel != null) {
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                String cardId = cardPayload.getCardId();
                Map<String, Object> params = new HashMap<>();
                params.put("default_source", cardId);
                customer.update(params); // Set this card is default
                LOGGER.info("setDefault {}", params);
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, params);
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    public CloudPayResponse del(CardPayload cardPayload) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(cardPayload.getExCustId(), cardPayload.getSystem());
            if (customerModel != null) {
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                DeletedExternalAccount source = customer.getSources().retrieve(cardPayload.getCardId()).delete();
                source.setLastResponse(null);// Trick to avoid JSON deserializer exception
                LOGGER.info("del {}", source);
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, source);
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    public CloudPayResponse del(BasePayLoad basePayLoad) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(basePayLoad.getExCustId(), basePayLoad.getSystem());
            if (customerModel != null) {
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                DeletedCustomer deletedCustomer = customer.delete();
                LOGGER.info("del {}", deletedCustomer);
                customerPayDao.del(customerModel);
                deletedCustomer.setLastResponse(null); // Trick to avoid JSON deserializer exception
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, deletedCustomer);
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    public CloudPayResponse list(BasePayLoad basePayLoad) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(basePayLoad.getExCustId(), basePayLoad.getSystem());
            if (customerModel != null) {
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, extractCards(customer));
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    private List<Card> extractCards(Customer customer) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
        String cardId = customer.getDefaultSource();
        Map<String, Object> cardParams = new HashMap<>();
        cardParams.put("object", "card");
        ExternalAccountCollection externalAccountCollection = customer.getSources().list(cardParams);
        LOGGER.info("list {}", externalAccountCollection);

        List<Card> cards = new ArrayList<>();
        for (ExternalAccount card : externalAccountCollection.getData()) {
            if (!card.getId().equals(cardId)) { // Exclude default card
                cards.add((Card) card);
            }
        }

        return cards;
    }

    public CloudPayResponse charge(ChargeCardPayload chargeCardPayload) {
        final Integer LOCKED = 1;
        final Integer UN_LOCKED = 0;
        CustomerModel customerModel;
        BlockerModel blockerModel = blockerDao.get(chargeCardPayload.getSystem(), chargeCardPayload.getExCustId());
        if (chargeCardPayload.getDescription().contains("Auto recharge")) {
            if (blockerModel != null && blockerModel.getFailed() >= MAX_FAILED) {
                return new CloudPayResponse(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.toString(), "Customer has been blocked due to auto recharge failed 3 times!", null);
            }
        }

        try {
            String lockedBy = RandomStringUtils.random(10, true, true);
            customerPayDao.lock(chargeCardPayload.getExCustId(), chargeCardPayload.getSystem(), LOCKED, lockedBy); // Lock record
            customerModel = customerPayDao.getAppropriateCustomer(chargeCardPayload.getExCustId(), chargeCardPayload.getSystem());
            if (customerModel != null) {
                if (LOCKED.equals(customerModel.getLocked()) && !lockedBy.equals(customerModel.getLockedBy())) {
                    return new CloudPayResponse(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.toString(), "Don't proceed too fast", null);
                } else {
                    Customer customer = Customer.retrieve(customerModel.getCustomerId());
                    Charge charge = chargeMultipleCards(chargeCardPayload, customer);

                    customerPayDao.charge(charge.getId(), customer.getId(), chargeCardPayload.getExCustId(), chargeCardPayload.getSystem());

                    charge.setLastResponse(null);// Trick to avoid JSON deserializer exception
                    LOGGER.info("charge {}", charge);

                    Card card = (Card) charge.getSource();
                    Card source = (Card) customer.getSources().retrieve(card.getId());
                    ChargeInfo chargeInfo = new ChargeInfo(charge);
                    chargeInfo.setCardName(card.getName());
                    chargeInfo.setCardEmail(source.getMetadata().get("email"));
                    chargeInfo.setCardPhone(source.getMetadata().get("phone"));
                    chargeInfo.setCardBrand(card.getBrand());
                    chargeInfo.setCardLast4(card.getLast4());
                    chargeInfo.setCardZip(source.getAddressZip());
                    if ("prod".equals(env)) {
                        SlackHelper.sendException(SlackChannel.Sale,
                                String.format("Charged $%,.2f to customerId: %d from %s system",
                                        (float) chargeCardPayload.getAmount() / 100,
                                        customerModel.getExtCustomerId(),
                                        chargeCardPayload.getSystem()),
                                String.format((charge.getDescription().contains("Auto recharge") ? "AUTO RECHARGE\n" : "NEW CLOUD SALE\n") +
                                                "\n" +
                                                "Description: %s\n" +
                                                "Transaction Amount: $%,.2f\n" +
                                                "Transaction ID: %s\n" +
                                                "\n" +
                                                "User Name: %s\n" +
                                                "User Email: %s\n" +
                                                "User Phone(s): %s\n" +
                                                "\n" +
                                                "Billing Name: %s\n" +
                                                "Billing Email: %s\n" +
                                                "Billing Phone: %s\n" +
                                                "\n" +
                                                "Seller message: %s\nCard: %s **** **** **** %s\nAddress zip: %s",
                                        chargeInfo.getDescription(), (float) chargeCardPayload.getAmount() / 100, chargeInfo.getId(),
                                        chargeCardPayload.getUsername(), chargeCardPayload.getEmail(), chargeCardPayload.getPhone(),
                                        chargeInfo.getCardName(), chargeInfo.getCardEmail(), chargeInfo.getCardPhone(),
                                        chargeInfo.getSellerMessage(), chargeInfo.getCardBrand(), chargeInfo.getCardLast4(), chargeInfo.getCardZip()));
                    }

                    if (blockerModel != null) {
                        blockerDao.del(blockerModel);
                    }

                    return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, chargeInfo);
                }
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            if (blockerModel == null) {
                blockerModel = new BlockerModel();
                blockerModel.setFailed(1);
                blockerModel.setExtCustomerId(chargeCardPayload.getExCustId());
                blockerModel.setSystem(chargeCardPayload.getSystem());
            } else {
                blockerModel.setFailed(blockerModel.getFailed() + 1);
            }
            blockerDao.save(blockerModel);

            e.printStackTrace();
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, String.format("ERROR:\n%s", ExceptionUtils.getRootCauseMessage(e)), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        } finally {
            customerPayDao.lock(chargeCardPayload.getExCustId(), chargeCardPayload.getSystem(), UN_LOCKED, ""); // Release lock
        }
    }

    private Charge chargeMultipleCards(ChargeCardPayload chargeCardPayload, Customer customer) throws APIException, AuthenticationException, InvalidRequestException, APIConnectionException, CardException {
        Charge charge = null;
        CardException exception = null;
        // Charge default card first
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", chargeCardPayload.getAmount());
        chargeParams.put("currency", chargeCardPayload.getCurrency());
        chargeParams.put("customer", customer.getId());
        chargeParams.put("description", chargeCardPayload.getDescription());
        try {
            charge = Charge.create(chargeParams);
        } catch (CardException e) {
            exception = e;
            LOGGER.info("Charge card failed: " + e.getCode());
            LOGGER.info("Message is: " + e.getMessage());

            List<Card> allCards = extractCards(customer); // Default card was failed, list all alternative cards

            for (Card card : allCards) {
                chargeParams.put("source", card.getId());

                try {
                    charge = Charge.create(chargeParams);
                    if (charge != null) {
                        return charge;
                    }
                } catch (CardException ce) {
                    LOGGER.info("Charge card failed: " + ce.getCode());
                    LOGGER.info("Message is: " + ce.getMessage());
                    exception = ce;
                }
            }
        }

        if (charge == null) throw exception;

        return charge;
    }

    public CloudPayResponse his(BasePayLoad basePayLoad) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(basePayLoad.getExCustId(), basePayLoad.getSystem());
            if (customerModel != null) {
                Customer customer = Customer.retrieve(customerModel.getCustomerId());
                Map<String, Object> customerParams = new HashMap<>();
                customerParams.put("customer", customer.getId());
                ChargeCollection chargeCollection = Charge.list(customerParams);
                LOGGER.info("hist {}", chargeCollection);
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, chargeCollection.getData());
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    public CloudPayResponse hisPaging(HisPagingPayLoad hisPagingPayLoad) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(hisPagingPayLoad.getExCustId(), hisPagingPayLoad.getSystem());
            if (customerModel != null) {
                List<ChargeHistoryModel> list = chargeDao.list(hisPagingPayLoad.getExCustId(), hisPagingPayLoad.getSystem(), hisPagingPayLoad.getFrom(), hisPagingPayLoad.getTo());
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, extractCharge(list));
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }


    public CloudPayResponse hisAll(BasePayLoad basePayLoad) {
        try {
            CustomerModel customerModel = customerPayDao.getAppropriateCustomer(basePayLoad.getExCustId(), basePayLoad.getSystem());
            if (customerModel != null) {
                List<ChargeHistoryModel> list = chargeDao.list(basePayLoad.getExCustId(), basePayLoad.getSystem());
                return new CloudPayResponse(HttpStatus.OK.value(), HttpStatus.OK.toString(), null, extractCharge(list));
            } else {
                return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), "Customer doesn't exist!", null);
            }
        } catch (Exception e) {
            LOGGER.error("ERROR" + ExceptionUtils.getRootCauseMessage(e));
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getRootCauseMessage(e), ExceptionUtils.getRootCauseMessage(e));
            return new CloudPayResponse(HttpStatus.FAILED_DEPENDENCY.value(), HttpStatus.FAILED_DEPENDENCY.toString(), ExceptionUtils.getRootCauseMessage(e), null);
        }
    }

    private List<Charge> extractCharge(List<ChargeHistoryModel> list) throws InterruptedException {
        final List<Charge> charges = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)) {
            ExecutorService executor = Executors.newFixedThreadPool(10);

            final List<Callable<Void>> callables = new ArrayList<>();
            for (ChargeHistoryModel model : list) {
                callables.add(() -> {
                    try {
                        Charge charge = Charge.retrieve(model.getChargeId());
                        charge.setLastResponse(null);
                        charges.add(charge);
                    } catch (Exception e) {
                        LOGGER.error("ERROR " + ExceptionUtils.getRootCauseMessage(e));
                    }
                    return null;
                });
            }

            executor.invokeAll(callables);
            executor.shutdown();
            charges.sort(Comparator.comparing(Charge::getCreated).reversed());
        }
        return charges;
    }

    public Map<String, String> collectSaleReportDaily(String system) {
        Map<String, String> data = new HashMap<>();
        try {
            List<ChargeHistoryModel> list = chargeDao.list(system);
            if (list != null) {
                LOGGER.info("Total charge history: " + list.size() + " times.");
                long twodays = 0;
                long today = 0;
                long sevenDays = 0;
                long fourteenDays = 0;
                long thirtyDays = 0;
                long sixtyDays = 0;
                long ninetyDays = 0;
                long alltime = 0;

                for (Charge charge : extractCharge(list)) {
                    if (charge != null) {
                        Date chargeDate = new Date(charge.getCreated() * 1000);
                        long amount = charge.getAmount();

                        alltime += amount;
                        if (isWithin(2, chargeDate)) {
                            twodays += amount;
                        }
                        if (isWithin(1, chargeDate)) {
                            today += amount;
                        }
                        if (isWithin(7, chargeDate)) {
                            sevenDays += amount;
                        }
                        if (isWithin(14, chargeDate)) {
                            fourteenDays += amount;
                        }
                        if (isWithin(30, chargeDate)) {
                            thirtyDays += amount;
                        }
                        if (isWithin(60, chargeDate)) {
                            sixtyDays += amount;
                        }
                        if (isWithin(90, chargeDate)) {
                            ninetyDays += amount;
                        }
                    }
                }

                float todayCompare = (float) today / (twodays - today) * 100 - 100;
                String todayStr = String.format((today * 2 > twodays ? "(\u2191" : "(\u2193") + " %.0f%% compare with yesterday)", todayCompare);
                data.put("today", String.format("Today: $%,.2f %s", (float) today / 100, todayStr));

                float lastweekCompare = (float) sevenDays / (fourteenDays - sevenDays) * 100 - 100;
                String lastweekStr = String.format((sevenDays * 2 > fourteenDays ? "(\u2191" : "(\u2193") + " %.0f%% compare with last week)", lastweekCompare);
                data.put("sevenDays", String.format("Last 7 days: $%,.2f %s", (float) sevenDays / 100, lastweekStr));

                float lastmonthCompare = (float) thirtyDays / (sixtyDays - thirtyDays) * 100 - 100;
                String lastmonthStr = String.format((thirtyDays * 2 > sixtyDays ? "(\u2191" : "(\u2193") + " %.0f%% compare with last month)", lastmonthCompare);
                data.put("thirtyDays", String.format("Last 30 days: $%,.2f %s", (float) thirtyDays / 100, lastmonthStr));

                data.put("sixtyDays", String.format("Last 60 days: $%,.2f", (float) sixtyDays / 100));
                data.put("ninetyDays", String.format("Last 90 days: $%,.2f", (float) ninetyDays / 100));
                data.put("alltime", String.format("All-Time: $%,.2f", (float) alltime / 100));
            }
        } catch (Exception e) {
            e.printStackTrace();
            SlackHelper.sendException(SlackChannel.Exception, ExceptionUtils.getMessage(e), ExceptionUtils.getMessage(e));
        }

        return data;
    }

    private boolean isWithin(int days, Date milestone) {
        Calendar range = Calendar.getInstance();
        range.add(Calendar.DATE, -days);
        return range.getTime().before(milestone);
    }
}
