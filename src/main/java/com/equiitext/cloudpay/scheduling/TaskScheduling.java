package com.equiitext.cloudpay.scheduling;

import com.equiitext.cloudpay.dao.dto.DailySaleReportPayload;
import com.equiitext.cloudpay.domain.CustomerService;
import com.equiitext.cloudpay.infrastructure.slack.SlackChannel;
import com.equiitext.cloudpay.infrastructure.slack.SlackHelper;
import com.google.gson.Gson;
import net.javacrumbs.shedlock.core.SchedulerLock;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@EnableScheduling
@Component
@Profile({"prod"})
public class TaskScheduling {
    private final static Logger LOGGER = LoggerFactory.getLogger(TaskScheduling.class);

    @Autowired
    private CustomerService customerService;

    @Value("${ext.salereport.endpoint}")
    private String extEndpoint;
    @Value("${application.self.key}")
    private String apikey;

    @Scheduled(cron = "0 0 7 1/1 * ?") // Everyday at 07:00 GMT = midnight PDT
    @SchedulerLock(name = "saleReportDaily", lockAtMostFor = 900, lockAtLeastFor = 900)
    public void saleReportDaily() throws IOException {
        LOGGER.info("saleReportDaily-started");
        Map<String, String> data = customerService.collectSaleReportDaily("cloud");
        String message = data.get("today") +
                "\n" +
                data.get("sevenDays") +
                "\n" +
                data.get("thirtyDays") +
                "\n" +
                data.get("sixtyDays") +
                "\n" +
                data.get("ninetyDays") +
                "\n" +
                data.get("alltime");

        LOGGER.info("Sending mail...");
        // Send request to external system
        DailySaleReportPayload parameter = new DailySaleReportPayload();
        parameter.setKey(apikey);
        parameter.setContent(message);
        parameter.setAttributes(data);
        OkHttpClient httpClient = new OkHttpClient();
        Gson gson = new Gson();
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json"), gson.toJson(parameter));
        Request request = new Request.Builder()
                .url(extEndpoint)
                .post(body)
                .addHeader("content-type", "application/json")
                .build();

        httpClient.newCall(request).execute();
        LOGGER.info("saleReportDaily-stopped");
    }
}
