package com.equiitext.cloudpay.infrastructure;

import com.equiitext.cloudpay.domain.exception.ErrorInfo;
import com.equiitext.cloudpay.domain.exception.CloudPayException;
import com.equiitext.cloudpay.infrastructure.slack.SlackChannel;
import com.equiitext.cloudpay.infrastructure.slack.SlackHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Quang on 02/11/17.
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    ErrorInfo handleRestException(HttpServletResponse response, CloudPayException ex) {
        LOG.error("Returning [" + ex.getHttpErrorCode() + "] " + ex.getMessage());
        response.setStatus(ex.getHttpErrorCode().value());
        SlackHelper.sendException(SlackChannel.Exception, ex.getMessage(), ex.getCause().toString());
        return new ErrorInfo(ex.getMessage(), ex.getHttpErrorCode(), ex.getKey());
    }
}