package com.equiitext.cloudpay.infrastructure.db.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "shedlock")
@Getter
@Setter
public class ShedLockModel {
    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "lock_until")
    private Timestamp lockUntil;

    @Column(name = "locked_at")
    private Timestamp lockedAt;

    @Column(name = "locked_by")
    private String lockedBy;
}
