package com.equiitext.cloudpay.infrastructure.db.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "blocker")
@Getter
@Setter
public class BlockerModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "ext_customer_id")
    private Long extCustomerId;
    @Column(name = "failed")
    private Integer failed;
    @Column(name = "system")
    private String system;
}
