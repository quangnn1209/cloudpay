package com.equiitext.cloudpay.infrastructure.db;

import com.equiitext.cloudpay.infrastructure.db.model.ChargeHistoryModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;


@Transactional
@Repository
@Profile({"beta", "prod", "test"})
public class ChargeDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(ChargeDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public List<ChargeHistoryModel> list(String system) {
        Query query = entityManager.createQuery("FROM ChargeHistoryModel as c WHERE c.system=:system");
        query.setParameter("system", system);
        return query.getResultList();
    }

    public List<ChargeHistoryModel> list(long extCustId, String system) {
        Query query = entityManager.createQuery("FROM ChargeHistoryModel as c WHERE c.system=:system AND c.extCustomerId=:extCustId");
        query.setParameter("system", system);
        query.setParameter("extCustId", extCustId);
        return query.getResultList();
    }

    public List<ChargeHistoryModel> list(long extCustId, String system, Date from, Date to) {
        Query query = entityManager.createQuery("FROM ChargeHistoryModel as c WHERE c.system=:system AND c.extCustomerId=:extCustId AND c.created >= :from AND c.created < :to");
        query.setParameter("system", system);
        query.setParameter("extCustId", extCustId);
        query.setParameter("from", from);
        query.setParameter("to", to);
        return query.getResultList();
    }

    public void del(int id, String system) {
        Query query = entityManager.createQuery("DELETE FROM ChargeHistoryModel as c WHERE c.id=:id AND c.system=:system");
        query.setParameter("system", system);
        query.setParameter("id", id);
        query.executeUpdate();
    }
}
