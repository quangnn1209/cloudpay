package com.equiitext.cloudpay.infrastructure.db;

import com.equiitext.cloudpay.infrastructure.db.model.BlockerModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Transactional
@Repository
@Profile({"beta", "prod", "test"})
public class BlockerDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(BlockerDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public void save(BlockerModel model) {
        entityManager.merge(model);
    }

    public void del(BlockerModel model) {
        entityManager.remove(model);
    }

    public BlockerModel get(String system, Long extCustId) {
        Query query = entityManager.createQuery("FROM BlockerModel b WHERE b.system=:system AND b.extCustomerId=:extCustId");
        query.setParameter("system", system);
        query.setParameter("extCustId", extCustId);
        List<BlockerModel> result = query.getResultList();
        if(!CollectionUtils.isEmpty(result)) {
            return result.get(0);
        } else {
            return null;
        }
    }
}
