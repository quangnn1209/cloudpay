package com.equiitext.cloudpay.infrastructure.db;

import com.equiitext.cloudpay.infrastructure.db.model.ChargeHistoryModel;
import com.equiitext.cloudpay.infrastructure.db.model.CustomerModel;
import com.stripe.Stripe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Transactional
@Repository
@Profile({"beta", "prod", "test"})
public class CustomerPayDao {
    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerPayDao.class);

    @Value("${application.stripeapi.key}")
    private String stripeApiKey;
    @PersistenceContext
    private EntityManager entityManager;

    @PostConstruct
    public void init() {
        Stripe.apiKey = this.stripeApiKey;
        LOGGER.info("Loaded Stripe key {}", this.stripeApiKey);
    }

    public CustomerModel getAppropriateCustomer(Long exCustId, String system) {
        Query query = entityManager.createQuery("FROM CustomerModel as c WHERE c.extCustomerId=:custId AND c.system=:system");
        query.setParameter("custId", exCustId);
        query.setParameter("system", system);
        List result = query.getResultList();
        if (result == null || query.getResultList().isEmpty()) {
            return null;
        }
        LOGGER.info("getAppropriateCustomer {}", result);
        return (CustomerModel) result.get(0);
    }

    public CustomerModel save(Long exCustId, String custId, String system) {
        CustomerModel model = new CustomerModel();
        model.setExtCustomerId(exCustId);
        model.setCustomerId(custId);
        model.setSystem(system);
        entityManager.persist(model);
        LOGGER.info("save {}", model);
        return model;
    }

    public CustomerModel save(CustomerModel model) {
        entityManager.merge(model);
        LOGGER.info("save {}", model);
        return model;
    }

    public void lock(Long exCustId, String system, Integer locked, String lockedBy) {
        LOGGER.info("Locking " + exCustId + "-" + lockedBy);
        Query query = entityManager.createQuery("UPDATE CustomerModel SET lockedBy=:lockedBy, locked=:locked WHERE extCustomerId=:custId AND system=:system");
        query.setParameter("custId", exCustId);
        query.setParameter("system", system);
        query.setParameter("lockedBy", lockedBy);
        query.setParameter("locked", locked);
        query.executeUpdate();
    }

    public void del(CustomerModel model) {
        entityManager.remove(model);

        Query query = entityManager.createQuery("DELETE FROM ChargeHistoryModel as c WHERE c.customerId=:customerId AND c.system=:system AND c.extCustomerId=:extCustomerId");
        query.setParameter("customerId", model.getCustomerId());
        query.setParameter("extCustomerId", model.getExtCustomerId());
        query.setParameter("system", model.getSystem());
        query.executeUpdate();
        LOGGER.info("del {}", model);
    }

    public ChargeHistoryModel charge(String chargeId, String customerId, Long extCustId, String system) {
        ChargeHistoryModel model = new ChargeHistoryModel();
        model.setChargeId(chargeId);
        model.setCustomerId(customerId);
        model.setExtCustomerId(extCustId);
        model.setSystem(system);
        entityManager.persist(model);
        LOGGER.info("charge {}", model);
        return model;
    }
}
