package com.equiitext.cloudpay.infrastructure.db.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="charge_history")
@Getter
@Setter
public class ChargeHistoryModel implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="id")
    private Integer id;
    @Column(name="ext_customer_id")
    private Long extCustomerId;
    @Column(name="customer_id")
    private String customerId;
    @Column(name="charge_id")
    private String chargeId;
    @Column(name="system")
    private String system;
    @Column(name="created")
    private Date created;

    @PrePersist
    void createdAt() {
        this.created = new Date();
    }
}
