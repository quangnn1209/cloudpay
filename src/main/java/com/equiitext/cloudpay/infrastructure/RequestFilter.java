package com.equiitext.cloudpay.infrastructure;

import com.equiitext.cloudpay.application.CloudPayApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestFilter implements Filter {
    @Autowired
    private CloudPayApplicationService cloudpayApplicationService;

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest parsedReq = (HttpServletRequest) request;
        String key = request.getParameter("key");
        if (cloudpayApplicationService.validateKey(key) || "/".equals(parsedReq.getRequestURI())) {
            chain.doFilter(request, response);
        } else {
            HttpServletResponse newResponse = (HttpServletResponse) response;
            newResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            newResponse.getWriter().write("Wrong way! Come back!");
            newResponse.getWriter().flush();
            newResponse.getWriter().close();
        }
    }

    @Override
    public void destroy() {

    }
}
