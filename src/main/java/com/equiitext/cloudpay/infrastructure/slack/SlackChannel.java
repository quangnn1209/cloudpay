package com.equiitext.cloudpay.infrastructure.slack;

public enum SlackChannel {
    Exception("https://hooks.slack.com/services/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"),
    Sale("https://hooks.slack.com/services/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");

    private String channel;

    SlackChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }
}
