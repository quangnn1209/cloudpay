package com.equiitext.cloudpay.dao.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Getter
@Setter
public class DailySaleReportPayload implements Serializable{
    private String key;
    private String content;
    private Map<String, String> attributes;
}
