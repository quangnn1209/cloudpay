package com.equiitext.cloudpay.dao.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChargeCardPayload extends BasePayLoad{
    private String currency;
    private Long amount;
    private String description;
    private String username;
    private String email;
    private String phone;
}