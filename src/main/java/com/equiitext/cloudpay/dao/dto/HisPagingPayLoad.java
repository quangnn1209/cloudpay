package com.equiitext.cloudpay.dao.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class HisPagingPayLoad extends BasePayLoad {
    private Date from;
    private Date to;
}
