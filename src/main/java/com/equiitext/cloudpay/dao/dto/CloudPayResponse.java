package com.equiitext.cloudpay.dao.dto;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Quang on 24/10/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect
public class CloudPayResponse<T> {
    private int status;
    private String code;
    private String message;
    private T content;
}