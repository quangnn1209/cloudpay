package com.equiitext.cloudpay.dao.dto;

import com.stripe.model.Charge;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Quang on 8/28/2018.
 */
@Getter
@Setter
public class ChargeInfo extends Charge {
    private String cardEmail;
    private String cardPhone;
    private String cardName;
    private String cardBrand;
    private String cardLast4;
    private String cardZip;
    private String sellerMessage;

    public ChargeInfo(Charge charge) {
        this.setId(charge.getId());
        this.setDescription(charge.getDescription());
        this.setSellerMessage(charge.getOutcome().getSellerMessage());
    }
}
