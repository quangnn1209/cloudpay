package com.equiitext.cloudpay.presentation;

import com.equiitext.cloudpay.dao.dto.*;
import com.equiitext.cloudpay.domain.CustomerService;
import com.equiitext.cloudpay.scheduling.TaskScheduling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by Quang on 31/10/17.
 */
@RestController
public class CloudPayController {
    @Autowired
    private CustomerService customerService;

    @RequestMapping(path = "/list-card", method = RequestMethod.POST)
    @ResponseBody
    public CloudPayResponse listCard(@RequestBody BasePayLoad request) {
        return customerService.list(request);
    }

    @RequestMapping(path = "/his-charge", method = RequestMethod.POST)
    @ResponseBody
    public CloudPayResponse hisCharge(@RequestBody BasePayLoad request) {
        return customerService.his(request);
    }

    @RequestMapping(path = "/his-charge-all", method = RequestMethod.POST)
    @ResponseBody
    public CloudPayResponse hisChargeAll(@RequestBody BasePayLoad request) {
        return customerService.hisAll(request);
    }

    @RequestMapping(path = "/his-charge-paging", method = RequestMethod.POST)
    @ResponseBody
    public CloudPayResponse hisChargePaging(@RequestBody HisPagingPayLoad request) {
        return customerService.hisPaging(request);
    }

    @RequestMapping(path = "/add-card", method = RequestMethod.POST)
    public CloudPayResponse addCard(@RequestBody NewCardPayload newCardPayload) {
        return customerService.create(newCardPayload);
    }

    @RequestMapping(path = "/get-card", method = RequestMethod.POST)
    public CloudPayResponse getCard(@RequestBody CardPayload cardPayload) {
        return customerService.get(cardPayload);
    }

    @RequestMapping(path = "/del-card", method = RequestMethod.POST)
    public CloudPayResponse delCard(@RequestBody CardPayload cardPayload) {
        return customerService.del(cardPayload);
    }

    @RequestMapping(path = "/del-cust", method = RequestMethod.POST)
    public CloudPayResponse delCust(@RequestBody BasePayLoad basePayLoad) {
        return customerService.del(basePayLoad);
    }

    @RequestMapping(path = "/set-card-default", method = RequestMethod.POST)
    public CloudPayResponse setCardDefault(@RequestBody CardPayload cardPayload) {
        return customerService.setDefault(cardPayload);
    }

    @RequestMapping(path = "/charge-card", method = RequestMethod.POST)
    public CloudPayResponse chargeCard(@RequestBody ChargeCardPayload chargeCardPayload) {
        return customerService.charge(chargeCardPayload);
    }

    @Autowired
    private TaskScheduling taskScheduling;

    @RequestMapping(path = "/daily-report", method = RequestMethod.GET)
    public String dailyReport() throws IOException {
        taskScheduling.saleReportDaily();
        return "OK";
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String root() {
        return "Welcome to deep web!";
    }
}
