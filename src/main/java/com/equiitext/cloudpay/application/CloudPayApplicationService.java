package com.equiitext.cloudpay.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by Quang on 24/10/17.
 */
@Service
public class CloudPayApplicationService {
    @Value("${application.self.key}")
    private String apikey;

    public boolean validateKey(String apikey){
        return this.apikey.equals(apikey);
    }
}