package com.equiitext.cloudpay;

import com.equiitext.cloudpay.dao.dto.ChargeCardPayload;
import com.equiitext.cloudpay.dao.dto.CloudPayResponse;
import com.equiitext.cloudpay.domain.CustomerService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CustomerServiceTest {
    @Autowired
    CustomerService customerService;

    @Ignore
    @Test
    public void testParallelCharge() throws InterruptedException, ExecutionException {
        ChargeCardPayload request = new ChargeCardPayload();
        request.setAmount(1_000l);
        request.setCurrency("usd");
        request.setDescription("Testing");
        request.setEmail("quang@gmail.com");
        request.setPhone("1234567890");
        request.setUsername("quang");
        request.setExCustId(33l);
        request.setSystem("cloud");

        ExecutorService executor = Executors.newFixedThreadPool(10);
        final List<Callable<CloudPayResponse>> callables = new ArrayList<>();
        final int totalRequest = 10;
        for (int i = 0; i < totalRequest; i++) {
            callables.add(() -> customerService.charge(request));
        }

        long now = System.currentTimeMillis();
        List<Future<CloudPayResponse>> results = executor.invokeAll(callables);
        long finished = System.currentTimeMillis();
        int blocked = 0;
        int passed = 0;
        for (Future<CloudPayResponse> result : results) {
            CloudPayResponse response = result.get();
            if (response.getStatus() == 403) {
                blocked++;
            } else {
                passed++;
            }
        }

        System.out.println("Send speed: " + totalRequest * 1000 / (finished - now) + "qps\nBlocked: " + blocked + ", Passed:" + passed + ". Blocked rate: " + (blocked * 100 / (blocked + passed)) + "%");
        executor.shutdown();
    }
}
